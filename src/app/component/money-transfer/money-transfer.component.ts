import { Component, OnInit } from '@angular/core';
import { appService } from '../../component/services/app.service';
import { Router, ActivatedRoute } from '@angular/router';

declare var $: any;
@Component({
  selector: 'app-money-transfer',
  templateUrl: './money-transfer.component.html',
  styleUrls: ['./money-transfer.component.css']
})

export class MoneyTransferComponent implements OnInit {
  tranferModel = {
    fromaccount: "selectaccount",
    toaccount: null,
    name: null,
    amount: null
  }

  accountNumbers = [
    { accountno: "123478679567", name: "saumya" },
    { accountno: "987689767543", name: "Lanchi" },
    { accountno: "908953212454", name: "Anil" },
    { accountno: "876675433465", name: "pooja" },
    { accountno: "985125675876", name: "shristi" }
  ]

  fromaccountNo: any
  fromaccountVali: boolean = false;
  toaccountVali: boolean = false;
  constructor(private service: appService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
  }
  toacc(val) {
    if (val != "selectaccount") {
      this.fromaccountVali = false
    }
  }
  onSubmit(data) {

    let value = data.toaccount.toString()
    // console.log("hsajgsjad",value.length)
    if (data.fromaccount == "selectaccount") {
      this.fromaccountVali = true
    }
    if (value.length != 12) {
      this.toaccountVali = true
    }
    else {
      this.toaccountVali = false;
    }

    if (this.fromaccountVali == false && this.toaccountVali == false) {
      console.log("value")
      this.service.setTransections(data);
      $('#myModal').modal('show');
    }

  }
  cancelModel() {
    this.router.navigate(["/ListofTransection"], { relativeTo: this.route });
  }

}
