import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoneyTransferComponent } from './component/money-transfer/money-transfer.component';
import { ListOfTransectionComponent } from './component/list-of-transection/list-of-transection.component';

const routes: Routes = [
  {
      path: 'SendMoney',
      component: MoneyTransferComponent,
     
  },
  {
      path: '',
      redirectTo: '/SendMoney',
      pathMatch: 'full'
  },
  {
      path: 'ListofTransection',
      component: ListOfTransectionComponent,
      
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
