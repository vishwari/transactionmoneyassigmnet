import { Component, OnInit } from '@angular/core';
import { appService } from '../../component/services/app.service';

@Component({
  selector: 'app-list-of-transection',
  templateUrl: './list-of-transection.component.html',
  styleUrls: ['./list-of-transection.component.css']
})
export class ListOfTransectionComponent implements OnInit {
  config: any;
  transctionValue: any = null;

  constructor(public service: appService) {
  }

  ngOnInit(): void {
    this.transctionValue = this.service.data;
    console.log(this.transctionValue.length);
// if(this.transctionValue.length)
    // for (var i = 0; i < this.service.data.length; i++) {
    //   this.collection.data.push(
    //     {
    //       id: i + 1,
    //       value: "items number " + (i + 1)
    //     }
    //   );
    // }

    this.config = {
      itemsPerPage: 5,
      currentPage: 1,
      totalItems: this.service.data.length
    };
  }
  pageChanged(event) {
    this.config.currentPage = event;
  }

}
