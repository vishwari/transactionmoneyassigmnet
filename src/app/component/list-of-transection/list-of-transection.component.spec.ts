import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfTransectionComponent } from './list-of-transection.component';

describe('ListOfTransectionComponent', () => {
  let component: ListOfTransectionComponent;
  let fixture: ComponentFixture<ListOfTransectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfTransectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfTransectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
