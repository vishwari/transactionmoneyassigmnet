import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MoneyTransferComponent } from './component/money-transfer/money-transfer.component';
import { ListOfTransectionComponent } from './component/list-of-transection/list-of-transection.component';
import { HeaderComponent } from './component/header/header.component';
import { appService } from './component/services/app.service';


@NgModule({
  declarations: [
    MoneyTransferComponent,
    ListOfTransectionComponent,
    HeaderComponent,
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    NgxPaginationModule
  ],
  providers: [appService],
  bootstrap: [AppComponent]
})
export class AppModule { }
